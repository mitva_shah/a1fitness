<?php

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/broadcast' , function() {
    event(new TestEvent('Broadcasting in laravel using Pusher!'));

    return view('pusher');
})->middleware('web');

Route::get('/bridge' , function() {
          $pusher = App::make('pusher');

          $pusher->trigger('test-channel' ,
                            'test-event',
                            array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

         return view('pusher');

  })->middleware('web');

Route::group(['prefix' => 'admin'], function () { Voyager::routes(); });



/*
Route::group(['middleware' => 'user'], function() {
  Route::get('/home', 'HomeController@index');
});
Route::get('/home', [
    'middleware' => 'admin.user',
    'uses' => 'HomeController@index'
]);

*/

Route::get('/', function () {
    return view('welcome');
})->middleware('frontend');

Auth::routes();

Route::post('register/customer' , 'Auth\RegisterController@registerCustomer');

Route::get('login/trainer' , 'Auth\RegisterController@registerCustomer');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::post('trainer/recurring/payment' , 'trainer\PaymentController@trainerExpressCheckout');

Route::get('trainer/payment/{price}/{user_id}' , 'trainer\PaymentController@createRecurringPaymentsProfile');

Route::post('trainer/payment/ipn' , 'trainer\PaymentController@ipn');

Route::group(['middleware' => ['auth' , 'frontend']], function() {

  Route::get('user/profile/{id}' , 'HomeController@profileCustomer');

  Route::get('user/profile/edit/{id}' , 'HomeController@profileCustomerEdit');

  Route::post('user/profile/edit/{id}' , 'HomeController@profileCustomerUpdate');

  Route::get('tests', 'MessageController@tests');

  Route::get('message' , 'MessageController@showUsersList');

  Route::get('message/{id}', 'MessageController@chatHistory')->name('message.read');
});


Route::group(['prefix'=>'ajax', 'as'=>'ajax::'], function() {
   Route::post('message/send', 'MessageController@ajaxSendMessage')->name('message.new');
   Route::delete('message/delete/{id}', 'MessageController@ajaxDeleteMessage')->name('message.delete');
});

Route::get('social/login/redirect/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('social/login/{provider}', 'Auth\AuthController@handleProviderCallback');


/*
* Videos can only be uploaded by Trainers, so add middleware trainer
*/

Route::group(['prefix'=>'customer' , 'middleware'=> ['auth' , 'customer']] , function() {
    Route::get('videos' , 'customer\VideoController@index');
    Route::get('play/video/{video_id}', 'customer\VideoController@play');
});

Route::group(['middleware'=>['auth','trainer']] , function() {
  Route::get('videos' , 'trainer\VideoController@index');

  Route::get('upload/video','trainer\VideoController@upload');

  Route::post('store/video/{trainer_id}','trainer\VideoController@store');

  Route::get('play/video/{video_id}', 'trainer\VideoController@play');

});

/*
* Filemanager package routes
*/
Route::group(['prefix' => 'filemanager','middleware' => 'auth'], function() {
    Route::get('index' , 'FilemanagerLaravelController@index');
    Route::get('show' , 'FilemanagerLaravelController@show');
    //Route::get('show' , 'FilemanagerLaravelController@getShow');
    Route::get('connectors', 'FilemanagerLaravelController@getConnectors');
    Route::post('connectors', 'FilemanagerLaravelController@postConnectors');
});

// pusher api code
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});
