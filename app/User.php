<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Authenticatable
{
    use Notifiable;
    use Messagable;

    const DEFAULT_IMAGE_PATH = "users/default.png";

    const ROLE_TRAINER = "trainer";

    const ROLE_CUSTOMER = "customer";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'height' , 'weight' , 'blood_group', 'role_id', 'avatar' ,'profile_id',
        'active', 'notification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


      public function conversations()
      {
        return $this->hasMany('App\Conversation','user_two','id');
      }

      public function role()
      {
        return $this->hasOne('App\Role','id','role_id');
      }
}
