<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Trainer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guest() && Auth::user()->role->name == Auth::user()::ROLE_TRAINER)
        {

          return $next($request);
        }

        return redirect(route('login'));
    }
}
