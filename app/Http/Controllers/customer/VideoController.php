<?php

namespace App\Http\Controllers\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class VideoController extends Controller
{
    public function index()
    {
      $video_list = array();
      $videos = Video::with('trainer')->get();

      // create an array with required values
      foreach($videos as $video) {
          $video_list[$video->id]['url'] = $video->url;
          $video_list[$video->id]['description'] = $video->description;
          $video_list[$video->id]['trainer'] = $video->trainer->name;
      }

      // get the current page from url , e.g., page 6
      $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Convert the video_list array in collection
          $collection = new Collection($video_list);

      // Define how many times we want to be visible on each page
        $perPage = 10;

        // Slice the collection to get items to display in current page
        $currentPageSearchResults = $collection->slice(($currentPage-1)*$perPage , $perPage)->all();


        // create our paginator and pass it to the view
        $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults , count($collection) , $perPage );


        return view('customer.video.index' , ['videos' => $paginatedSearchResults]);

      }

        /*
      * Play the selected video
      */
      public function play($video_id) {
          $video = Video::find($video_id);
          return view('customer.video.play', compact('video'));
      }

}
