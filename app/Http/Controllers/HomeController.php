<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use TCG\Voyager\Facades\Voyager;
use App\User;
use DB;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (!Auth::guest()) {
          $user = Voyager::model('User')->find(Auth::id());

          if($user->hasPermission('browse_frontend')) {
              return view('home');
          } else {
              //return redirect('/admin');
              return redirect('/');
          }
      }

        return view('home');
    }

    /**
     * Show the user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileCustomer($id)
    {
        $metadata = array();

        $user = User::find($id);

        //$avatar_src = url('/')."/storage/".$user->avatar;
        //$avatar_src = Storage::disk(config('app.storage.disk'))->get($user->avatar);
        $avatar_src = url('/').Storage::disk(config('app.storage.disk'))->url($user->avatar);

        $customer_fields = DB::table('customer_fields')
                        ->where('user_id','=',$id)
                        ->get();

          $metadata['name'] = $user->name;
          $metadata['email'] = $user->email;

          foreach($customer_fields as $customer_field) {
                    $meta_key = $customer_field->meta_key;
                    $meta_value = $customer_field->meta_value;
                    $metadata[$meta_key] = $meta_value;
          }

         $src = url('/')."/storage/".$user->avatar;


         return view('auth.profile_customer')
                  ->with([
                          'user_id' => $id,
                          'avatar_src'=>$avatar_src,
                          'metadata'=>$metadata
                        ]);
    }

    /**
     * Edit the user profile.
     *
     * @return \Illuminate\Http\Response
     */

     public function profileCustomerEdit($id)
     {
          $metadata = array();

          $user = User::find($id);

          $avatar_src = url('/')."/storage/".$user->avatar;



                $metadata['name'] = $user->name;
                $metadata['email'] = $user->email;

          $customer_fields = DB::table('customer_fields')
                              ->where('user_id','=',$id)
                              ->get();

          foreach($customer_fields as $customer_field )
          {
                $meta_key = $customer_field->meta_key;
                $meta_value = $customer_field->meta_value;

                $metadata[$meta_key] = $meta_value;
          }

          return view('auth.profile_customer_edit')
                  ->with([
                      'user_id' => $id,
                      'avatar_src'=>$avatar_src,
                      'metadata'=>$metadata
                    ]);
     }

     /**
      * Update the user profile.
      *
      * @return \Illuminate\Http\Response
      */
      public function profileCustomerUpdate($id , Request $request)
      {
          $user = User::find($id);

          $user->name = $request->name;
          $user->email = $request->email;


          $fullFilename = null;
          $resizeWidth = 1800;
          $resizeHeight = null;
          $file =  $request->file('avatar');

          if($file) {
            $filename = Str::random(20);
            $fullPath = 'users/'.date('F').date('Y').'/'.$filename.'.'.$file->getClientOriginalExtension();

          $image = Image::make($file)
              ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })
              ->encode($file->getClientOriginalExtension(), 75);

          // move uploaded file from temp to uploads directory
          if (Storage::disk(config('app.storage.disk'))->put($fullPath, (string) $image, 'public')) {
              $status = 'success';
              $fullFilename = $fullPath;

              $user->avatar = $fullFilename;

            /*
            $data = $request->all();
            $data['avatar'] = $fullFilename;
            $request->getInputSource()->replace($data);

              dd($data);
            */

          } else {
              $status = 'failure';
              redirect()->back();
          }

        }

          $user->save();

          $data = $request->all();

            foreach($data as $key=>$value) {
              if($key != 'name' && $key!= 'email' && $key != 'password' &&
              $key != 'avatar' && $key != 'role_id' && $key != '_token')
                 {
                  DB::table('customer_fields')
                    ->where('user_id','=',$id)
                    ->where('meta_key','=',$key)
                    ->update([
                        'meta_value'=>$value
                    ]);
                }
            }


            $notification = array(
                  'message' => 'Customer profile was updated.' ,
                  'alert-type'=>'success' ,
            );

            return redirect()->back()->with($notification);
      }
}
