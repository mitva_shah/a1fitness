<?php

namespace App\Http\Controllers\trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Video;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class VideoController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /*
  * Show all videos for the currently logged in trainer
  */
  public function index() {
      $videos = Video::all()->where('trainer_id' ,'=', Auth::user()->id);

      /*
      * Add Pagination
      */

      // get the current page from url , e.g, page 6
      $currentPage = LengthAwarePaginator::resolveCurrentPage();

      // Define how many times we want to be visible on each page
      $perPage = 10;

      // Slice the collection to get items to display in current page
      $currentPageSearchResults = $videos->slice(($currentPage-1)*$perPage , $perPage)->all();

      // create our paginator and pass it to the view
      $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($videos), $perPage);

      return view('video.index' , ['videos' => $paginatedSearchResults]);
  }

  /*
  * Upload a video
  */
  public function upload()
  {
      // get the currently logged in user id
      $user = Auth::user();
      return view('video.upload',compact('user'));
  }

  /*
  * Save an uploaded video
  */
  public function store(Request $request , $trainer_id)
  {
    $this->validate($request, [
         'file' => 'required|mimes:mp4,avi,asf,mov,qt,avchd,flv,swf,mpg,mpeg,mpeg-4,wmv,divx,3gp|max:20480',
     ]);

      if($request->hasFile('file'))  {
          $file = $request->file('file');
          $fileName  = Str::random(20).'.'.$file->getClientOriginalExtension();
          $fullPath = 'video_uploads/'.date('F').'_'.date('Y');
          $url = $file->move( $fullPath , $fileName );
      }
          //save the video in the database
            Video::create([
                'trainer_id' => $trainer_id,
                'description'=> $request->description,
                'url' => $url
            ]);

            $notification = [
              'message' => 'Video was uploaded.',
              'alert-type' => 'success'
            ];

            return redirect(url('videos'))->with($notification);
  }

  /*
  * Play the selected video
  */
  public function play($video_id) {
      $video = Video::find($video_id);
      return view('video.play', compact('video'));
  }

}
