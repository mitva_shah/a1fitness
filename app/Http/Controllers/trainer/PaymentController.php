<?php

namespace App\Http\Controllers\trainer;


use PayPal\Service\PayPalAPIInterfaceServiceService;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;
use PayPal\EBLBaseComponents\BillingAgreementDetailsType;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\EBLBaseComponents\RecurringPaymentsProfileDetailsType;
use PayPal\EBLBaseComponents\BillingPeriodDetailsType;
use PayPal\EBLBaseComponents\ScheduleDetailsType;
use PayPal\EBLBaseComponents\CreateRecurringPaymentsProfileRequestDetailsType;
use PayPal\EBLBaseComponents\ActivationDetailsType;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileRequestType;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileReq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use IpnListener;
use App\User;
use DB;
use Config;


class PaymentController extends Controller
{
    protected $config;

    public function __construct()
    {
      $config = array (
        'mode' =>    Config::get('paypal.settings.mode') ,
        'acct1.UserName' => Config::get('paypal.username'),
        'acct1.Password' => Config::get('paypal.password'),
        'acct1.Signature' => Config::get('paypal.signature')
      );

          $this->config = $config;
    }

      public function trainerExpressCheckout(Request $request)
      {
        $data = $request->all();
        if($data['avatar'] == null) {
           $data['avatar'] = User::DEFAULT_IMAGE_PATH;
        }

        $user = User::create([
                  'name' => $data['name'],
                  'email' => $data['email'],
                  'password' => bcrypt($data['password']),
                  'role_id' => $data['role_id'],
                  'avatar' => $data['avatar']
              ]);

          //create the meta data array
          foreach($data as $key=>$value) {
                if($key != 'name' && $key!= 'email' && $key != 'password' &&
                $key != 'avatar' && $key != 'role_id' && $key != '_token')
                {
                  DB::table('customer_fields')->insert(
                    [
                        'meta_key' => $key,
                        'meta_value' => $value,
                        'user_id' => $user->id
                    ]);
                }
          }

        $price = $request->price;
        $config = $this->config;

         $paypalService = new PayPalAPIInterfaceServiceService($config);
         $paymentDetails= new PaymentDetailsType();

         $orderTotal = new BasicAmountType();
         $orderTotal->currencyID = 'USD';
         $orderTotal->value = $price;//your own value

         $paymentDetails->OrderTotal = $orderTotal;
         $paymentDetails->PaymentAction = 'Sale';

         $setECReqDetails = new SetExpressCheckoutRequestDetailsType();
         $setECReqDetails->PaymentDetails[0] = $paymentDetails;
         $setECReqDetails->CancelURL = url('/');
         $setECReqDetails->ReturnURL = url('/trainer/payment/'.$price.'/'.$user->id);

         $billingAgreementDetails = new BillingAgreementDetailsType('RecurringPayments');
         $billingAgreementDetails->BillingAgreementDescription = 'Recurring Billing';
         $setECReqDetails->BillingAgreementDetails = array($billingAgreementDetails);

         $setECReqType = new SetExpressCheckoutRequestType();
         $setECReqType->Version = '104.0';
         $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;

         $setECReq = new SetExpressCheckoutReq();
         $setECReq->SetExpressCheckoutRequest = $setECReqType;

         $setECResponse = $paypalService->SetExpressCheckout($setECReq);

              //dd($setECResponse);
         if($setECResponse->Ack == 'Success') {
             $token = $setECResponse->Token;
             return Redirect::to('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$token.'&useraction=commit');
         } else {
             //return Redirect::to('error')->with('message', 'There is a problem in payment processing.');
             return redirect(url('/'))->with('error' , 'There was a problem in payment processing.');
         }
      }


      public function createRecurringPaymentsProfile(Request $request , $price , $user_id)
      {
            $token = $request->token;
            $payerId = $request->PayerID;

            $profileDetails = new RecurringPaymentsProfileDetailsType();
            $profileDetails->BillingStartDate = date(DATE_ISO8601, strtotime(date('Y-m-d').' +1 day'));
            //since start date should be greater than current date

            $paymentBillingPeriod = new BillingPeriodDetailsType();
            $paymentBillingPeriod->BillingFrequency = "1";
            $paymentBillingPeriod->BillingPeriod = "Month";

            $paymentBillingPeriod->Amount = new BasicAmountType("USD" , $price);

                  $scheduleDetails = new ScheduleDetailsType();
                  $scheduleDetails->Description = "recurring billing";
                  $scheduleDetails->PaymentPeriod = $paymentBillingPeriod;
                  $activationDetails = new ActivationDetailsType();
                  $activationDetails->InitialAmount = new BasicAmountType('USD' , $price);
                  $activationDetails->FailedInitialAmountAction = 'ContinueOnFailure';
                  $scheduleDetails->ActivationDetails = $activationDetails;

                  $createRPProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
                  $createRPProfileRequestDetails->Token = $token;

                  $createRPProfileRequestDetails->ScheduleDetails = $scheduleDetails;
                  $createRPProfileRequestDetails->RecurringPaymentsProfileDetails = $profileDetails;

                  $createRPProfileRequest = new CreateRecurringPaymentsProfileRequestType();
                  $createRPProfileRequest->CreateRecurringPaymentsProfileRequestDetails = $createRPProfileRequestDetails;

                  $createRPProfileReq = new CreateRecurringPaymentsProfileReq();
                  $createRPProfileReq->CreateRecurringPaymentsProfileRequest = $createRPProfileRequest;


                  $config = $this->config;
                  $paypalService = new PayPalAPIInterfaceServiceService($config);
                  $createRPProfileResponse = $paypalService->CreateRecurringPaymentsProfile($createRPProfileReq);

                      if($createRPProfileResponse->Ack == 'Success')  {
                          $profileId = $createRPProfileResponse->CreateRecurringPaymentsProfileResponseDetails->ProfileID;
                          //your own logic to save details in database.
                            $user = User::find($user_id);

                            $user->update([
                                'profile_id' => $profileId,
                                'active' => true,
                            ]);

                        //return redirect(url('/login'))->with('msg' , 'Payment was successful and Trainer was registered. Use the credentials used while registration to login.');

                        $notification = array(
                            'message' => 'Payment was successful and Trainer was registered. Use the credentials used while registration to login!',
                            'alert-type' => 'success'
                            );

                        //$this->guard()->login($user);
                      return redirect(url('/login'))->with($notification);


                       //return $this->registered($request, $user) ?: redirect($this->redirectPath())->with($notification);
                  } else  {
                    $notification = array(
                        'message' => 'There was a problem in payment processing.',
                        'alert-type' => 'error'
                        );

                        return redirect(url('/'))->with($notification);
                        //return Redirect::to('error')->with('message', 'There is a problem in payment processing.');
                  }
      }

          public function ipn()
          {
              $listener =  new IpnListener();

              $listener->use_sandbox = true;

              try {
                $verified = $listener->processIpn();
                } catch(Exception $e) {
                return Log::error($e->getMessage());
                }

              if($verified) {
                //ipn response was verified
                $data = $POST;

                $user_id = json_decode($data['custom'])->user_id;

                $subscription = ($data['mc_gross_1'] == '10') ? 2 : 1;

                $txn = array(
                    'txn_id' => $data['txn_id'],
                    'user_id' => $user_id,
                    'paypal_id' => $data['subscr_id'],
                    'subscription' => $subscription,
                    'expires' => date('Y-m-d H:i:s' , strtotime('+1 Month')),
                );

                Payment::create($txn);
              } else {
                //ipn response was not verified
                Log::error('Transaction not verified');
              }
          }
}
