<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Nahid\Talk\Facades\Talk;
use Auth;
use View;
use DB;
use App\Conversation;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Role;

class MessageController extends Controller
{
    protected $authUser;

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function($request, $next) {
              $user_id = Auth::user()->id;
              Talk::setAuthUserId($user_id);
              return $next($request);
        });

          View::composer('partials.peoplelist', function($view) {
                $thread_list = array();
                $threads = Talk::threads();

                foreach($threads as $inbox) {
                    $count = 0;
                          //dd($inbox->thread);
                    //dd(!is_null($inbox->thread));

                    if(!is_null($inbox->thread)) {

                          $thread_list[$inbox->withUser->id]['name'] = $inbox->withUser->name;
                          $thread_list[$inbox->withUser->id]['avatar'] = $inbox->withUser->avatar;
                          if(Auth::user()->id == $inbox->thread->sender->id) {
                              $thread_list[$inbox->withUser->id]['last_message'] = substr($inbox->thread->message, 0, 20);
                          } else {
                                $thread_list[$inbox->withUser->id]['last_message'] = null;
                          }

                          $all_messages = DB::table('messages')
                                      ->where('user_id','=',$inbox->thread->sender->id)
                                      ->get();

                          foreach($all_messages as $message) {
                            if($message->is_seen == 0 )
                            {
                                $count++;
                            }
                          }

                          $thread_list[$inbox->withUser->id]['unread_messages'] = $count;
                    }
                }
                $view->with(compact('threads','thread_list'));
          });
    }

    public function showUsersList()
    {
      // Get all trainers list if logged in user is a customer
      //$users = User::with(array('role'=> function($query){
          // $query->where('user','=','customer');
      //}))->get();
      $role_name = Auth::user()->role->name;

      if($role_name == Role::ROLE_TRAINER)
      {
        // Get all customers list if logged in user is a trainer
        $users = User::join('roles','roles.id','=','users.role_id')
              ->where('roles.name','=',Role::ROLE_CUSTOMER)
              ->select('users.id as user_id' , 'users.name','users.email','users.avatar')
              ->get();
      }

      if($role_name == Role::ROLE_CUSTOMER )
      {
        // Get all customers list if logged in user is a trainer
        $users = User::join('roles','roles.id','=','users.role_id')
              ->where('roles.name','=',Role::ROLE_TRAINER)
              ->select('users.id as user_id' , 'users.name','users.email','users.avatar')
              ->get();
      }

      $user_list = array();

      $conversations = DB::table('conversations')
                        ->where('user_two','=',Auth::user()->id)
                        ->where('notification', '=' ,1)
                        ->pluck('user_one')->toArray();

        foreach($users as $user) {
            $user_list[$user->user_id]['name'] = $user->name;
            $user_list[$user->user_id]['avatar'] = $user->avatar;
            $user_list[$user->user_id]['new_messages'] = null;
        }

        foreach($conversations as $conversation ){
            $user_list[$conversation]['new_messages'] = true;
        }

        // get the current page from url. e.g, page no. 6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new laravel collection from array data
        $collection = new Collection($user_list);

        // Define how many times we want to be visible on each page
        $perPage = 10;

        // Slice the collection to get items to display in current page
        $currentPageSearchResults = $collection->slice(($currentPage-1)*$perPage, $perPage)->all();

        // create our paginator and pass it to the view
        $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($collection) , $perPage );

        $notification = array(
                'message' => 'You have new messages',
                'users' => $conversations,
                'alert-type'=>'success'
        );

        return view('messages.index' , ['user_list' => $paginatedSearchResults])
          ->with($notification);
    }

    public function chatHistory($id)
    {
          $conversations = Talk::getMessagesByUserId($id);

          $user = '';

          $messages = [];

          if(!$conversations) {
            $user = User::find($id);
          } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
          }


            DB::table('conversations')
              ->where('user_two','=',Auth::user()->id)
              ->update([
                  'notification'=>false
              ]);

              //  update all messages as seen
                  foreach ($messages as $message) {
                      $message->update([
                            'is_seen'=> 1
                      ]);
                  }

            Auth::user()->update(['notification'=>false]);

          return view('messages.conversations',compact('messages','user'));
    }

    public function ajaxSendMessage(Request $request)
    {
            if($request->ajax()) {
              $rules = [
                  'message-data' => 'required',
                  '_id' => 'required'
              ];

              $this->validate($request , $rules);

              $body = $request->input('message-data');
              $userId = $request->input('_id');

              if($message = Talk::sendMessageByUserId($userId, $body)) {
                // update the column to show notification when user logs in
                  DB::table('users')
                    ->where('id','=',$userId)
                    ->update([
                        'notification' => true
                    ]);

                  DB::table('conversations')
                    ->where('user_one' ,'=', Auth::user()->id )
                    ->where('user_two' ,'=', $userId)
                    ->update([
                        'notification'=>true
                    ]);


                $html = view('ajax.newMessageHtml',compact('message'))->render();
                return response()->json(['status'=>'success', 'html' => $html],200);
              }
            }
    }

        public function ajaxDeleteMessage(Request $request , $id)
        {
            if($request->ajax())
            {
              if(Talk::deleteMessage($id)) {
                  return response()->json(['status' => 'success'],200);
              }

              return response()->json(['status'=>'errors','msg'=>'Something went wrong.'],401);
            }
        }


          public function tests()
          {
              //dd(Talk::channel());

            $b = new Broadcast(\Illuminate\Contracts\Config\Repository::class, \Vinkla\Pusher\PusherFactory::class);
            dd($b->tests());
          }
}
