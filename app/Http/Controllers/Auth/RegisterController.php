<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'register_as' => 'required',
            'avatar' => 'mimes:jpeg,jpg,png,gif,bmp',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


  /**
  * Override showRegistrationForm method
   * Show the application registration form.
   *
   * @return \Illuminate\Http\Response
   */
    public function showRegistrationForm()
    {
        //select the roles from the database
        $roles = DB::table('roles')->get();
        return view('auth.register_customer_trainer')->with('roles' , $roles);
    }

    /**
      * Override register method
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user_details = array();
        $user_details['name'] = $request->name;
        $user_details['email'] = $request->email;
        $user_details['password'] = $request->password;
        $user_details['role_id'] = $request->register_as;

        $file = $request->file('avatar');

            if($file) {
              $fullFilename = $this->addAvatar(null, 1800 , null ,$request->file('avatar'));

              $user_details['avatar'] = $fullFilename;
            } else {
              $user_details['avatar'] = null;
            }

          //check whether user has registered as customer or trainer
          //dd($request->register_as);

            $register_as = $request->register_as;
            //find the name associated with the id from roles table
            $role_name = DB::table('roles')->where('id','=',$register_as)->value('name');

            if($role_name == "trainer") {
              $default_package = array();
              $packages = array();

              $package_addons = DB::table('package_addons')->get();

              $default_package_fields = DB::table('config_fields')->get();

                  /*
                    foreach($default_package_fields as $default_package) {
                          $packages['default']['id'] = $default_package->id;
                          $packages['default']['title'] = $default_package->title;
                          $packages['default']['description'] = $default_package->description;
                          $packages['default']['price'] = $default_package->price;
                          $packages['default']['image'] = $default_package->image;
                    }
                    */

                    foreach($default_package_fields as $default_package_field)
                    {
                        $default_package[$default_package_field->meta_key] = $default_package_field->meta_value;
                    }

                    foreach($package_addons as $package_addon) {
                              $image_src = url('/')."/storage/".$package_addon->image;
                              $packages[$package_addon->id]['title'] = $package_addon->title;
                              $packages[$package_addon->id]['description'] = $package_addon->description;
                              $packages[$package_addon->id]['price'] = $package_addon->price;
                              $packages[$package_addon->id]['image'] = $image_src;
                    }

              return view('auth.register_trainer')
                      ->with([
                                'packages'=> $packages,
                                'default_package'=> $default_package,
                                'user_details' => $user_details
                        ]);

            } else if($role_name == "customer") {
              return view('auth.register_customer')->with('user_details',$user_details);
            } else {
              return redirect('/');
            }

        /**
        *event(new Registered($user = $this->create($request->all())));

        *$this->guard()->login($user);

        *return $this->registered($request, $user)
          *  ?: redirect($this->redirectPath());
          **/
    }

    /**
     * Handle a registration request for user type customer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function registerCustomer(Request $request)
     {
             $this->validate($request,[
                'weight' => 'required',
                'height' => 'required',
                'blood_group' => 'required',
             ]);

          /*
            $file = $request->file('avatar');
            if($file) {
              $fullFilename = $this->addAvatar(null, 1800 , null ,$request->file('avatar'));
              $request->avatar = $fullFilename;

              $request_array = $request->all();
              $request_array['avatar'] = $fullFilename;
            }
        event(new Registered($user = $this->createCustomer($request_array)));
        */
        event(new Registered($user = $this->createCustomer($request->all())));

        $notification = array(
            'message' => 'Customer was registered. Use the credentials used while registration to login.!',
            'alert-type' => 'success'
            );

        //$this->guard()->login($user);
          return redirect(url('/login'))->with($notification);
          //return $this->registered($request, $user) ?: redirect($this->redirectPath());
        //->with('message', 'Customer was registered. Use the credentials used while registration to login.');
     }

     /**
      * Create a new customer instance after a valid registration.
      *
      * @param  array  $data
      * @return User
      */
     public function createCustomer(array $data)
     {
           if($data['avatar'] == null) {
              $data['avatar'] = User::DEFAULT_IMAGE_PATH;
           }

          $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'role_id' => $data['role_id'],
                    'avatar' => $data['avatar'],
                    'active' => true,
                ]);

        //create the meta data array
        foreach($data as $key=>$value) {
              if($key != 'name' && $key!= 'email' && $key != 'password' &&
              $key != 'avatar' && $key != 'role_id' && $key != '_token')
              {
                DB::table('customer_fields')->insert(
                  [
                      'meta_key' => $key,
                      'meta_value' => $value,
                      'user_id' => $user->id
                  ]);
              }
        }

        return $user;
     }


   public function addAvatar($fullFilename , $resizeWidth , $resizeHeight , $file)
   {
       $filename = Str::random(20);
       $fullPath = 'users/'.date('F').date('Y').'/'.$filename.'.'.$file->getClientOriginalExtension();

       $ext = $file->guessClientExtension();

           $image = Image::make($file)
               ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                   $constraint->aspectRatio();
                   $constraint->upsize();
               })
               ->encode($file->getClientOriginalExtension(), 75);


           // move uploaded file from temp to uploads directory
           if (Storage::disk(config('app.storage.disk'))->put($fullPath, (string) $image, 'public')) {
              return $fullPath;
           }
   }
}
