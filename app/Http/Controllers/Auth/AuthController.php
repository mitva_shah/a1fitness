<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


//Add These three required namespace

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;


class AuthController extends Controller
{
    use AuthenticatesUsers;


    public function __construct()
    {
      $this->redirectPath = '/';
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        $data = [
              'name' => $user->getName(),
              'email' =>$user->getEmail()
        ];

        // Store data to the users TokyoTyrantTable
        User::firstOrCreate($data);


        // after login , redirect to the home page

        return redirect($this->redirectPath());
    }
}
