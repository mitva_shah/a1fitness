<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
              'id','trainer_id' ,'url','description'
    ];

    // each video was uploaded by a trainer, get that trainer.
    public function trainer()
    {
      return $this->hasOne('App\User','id','trainer_id');
    }
}
