<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
      const ROLE_TRAINER = "trainer";

      const ROLE_CUSTOMER = "customer";

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'name', 'display_name','id'
      ];
}
