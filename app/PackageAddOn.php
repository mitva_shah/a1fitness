<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageAddOn extends Model
{
    protected $table = "package_addons";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'title', 'description', 'price', 'image' ,
    ];
}
