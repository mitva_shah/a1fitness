<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
  protected $table = "conversations";


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
            'id', 'user_one', 'user_two', 'status' ,'notification',
  ];

      public function users()
      {
        return $this->belongsTo('App\User','user_two','id');
      }


}
