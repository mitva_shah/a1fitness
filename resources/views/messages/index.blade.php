@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Users</div>
                <div class="panel-body">
                            @foreach($user_list as $id=>$user)
                              <table class = "table">
                                <tr>
                                    <td>
                                        <img src = "{{ url('/').'/storage/'.$user['avatar'] }}"alt= "avatar" class = "avatar" />
                                        {{ $user['name'] }}
                                    </td>
                                    <td>
                                          @if($user['new_messages'])
                                            <div class = "alert alert-danger">
                                              {{ "You have new messages"}}
                                            </div>
                                          @endif
                                    </td>
                                    <td>
                                        <a href="{{route('message.read', ['id'=>$id])}}" class="btn btn-success pull-right">Send Message</a>
                                    </td>
                                </tr>
                              </table>
                            @endforeach
                </div>
            </div>
            <?php echo $user_list->setPath(url('message'))->render() ?>
        </div>
    </div>

</div>
@endsection
