@extends('layouts.main')


@section('content')
<div class="container">
    <div class="row register-form-wrap">
        <div class="col-md-10 col-md-offset-1 register-form">
            <div class="panel panel-default">
                <div class="panel-heading">User Profile</div>
                <div class="panel-body">

                            <div class = "row">
                            <label for="avatar" class="col-md-4 control-label">avatar</label>
                              <div class = "col-md-4">
                                <img src = "{{ $avatar_src }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                              </div>
                                <div class = "col-md-4">
                                  </div>
                            </div>

                              <br/>
                              <br/>

                              @foreach($metadata as $key=>$value)
                                <div class = "form-group">
                                    <label for = "{{ $key }}" class="col-md-4 control-label">{{ $key }}</label>
                                      <div>
                                          {{ $value }}
                                      </div>
                                </div>
                              @endforeach

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href = " {{ url('/user/profile/edit/'.$user_id) }}" type = "button" class = "btn btn-primary">
                                    Edit Profile
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
