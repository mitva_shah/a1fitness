  @extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Profile</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/profile/edit/'.$user_id) }}" enctype = "multipart/form-data">
                        {{ csrf_field() }}

                        <div class = "row">
                        <label for="avatar" class="col-md-4 control-label">avatar</label>
                          <div class = "col-md-4">
                            <img src = "{{ $avatar_src }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                          </div>

                            <div class = "col-md-4">

                              </div>
                        </div>

                        <br/>
                          <div class = "row">
                            <div class = "col-md-4">
                              </div>
                              <div class = "col-md-4">
                                  <input type = "file" name = "avatar"/>
                              </div>
                              <div class = "col-md-4">
                                </div>
                          </div>

                          <br/>
                         @foreach($metadata as $key=>$value)
                         <div class="form-group{{ $errors->has($key) ? ' has-error' : '' }}">
                             <label for="{{ $key }}" class="col-md-4 control-label">{{ $key }}</label>

                             <div class="col-md-6">
                                 <input id="{{ $key }}" type="text" class="form-control" name="{{ $key }}" value="{{ $value }}" required autofocus>

                                 @if ($errors->has($key))
                                     <span class="help-block">
                                         <strong>{{ $errors->first($key) }}</strong>
                                     </span>
                                 @endif
                             </div>
                         </div>
                         @endforeach

                         <div class="form-group">
                             <div class="col-md-6 col-md-offset-4">
                                 <button type="submit" class="btn btn-primary">
                                     Edit Profile
                                 </button>
                             </div>
                         </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
