@extends('layouts.app')

@section('bodyclass', 'auth-page register-page')

@section('content')
<div class="container">
    <div class="row register-form-wrap">
        <div class="col-md-6 col-md-offset-3 register-form">
          <div class="site-logo">
              <img src="{{ url('/images/light-logo.png') }}" />
          </div>
                  <div class="panel panel-trasparent">
                      <div class="panel-heading transparent-panel-heading">
                          <a class="login-icon"><i class="fa fa-user-plus"></i></a>
                          <h1>Customer Info</h1>
                      </div>
                      <div class = "panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register/customer') }}" enctype = "multipart/form-data">
                        {{ csrf_field() }}

                        <input id="name" type="hidden" class="form-control" name="name" value="{{ $user_details['name'] }}">
                        <input id="email" type="hidden" class="form-control" name="email" value="{{ $user_details['email'] }}">
                        <input id="password" type="hidden" class="form-control" name="password" value="{{ $user_details['password'] }}">
                        <input id="role_id" type="hidden" class="form-control" name ="role_id" value = "{{ $user_details['role_id'] }}">
                        <input id="avatar" type="hidden" class="form-control" name="avatar" value="{{ $user_details['avatar']  }}">

                        <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                            <label for="weight" class="col-sm-12">Weight</label>

                            <div class="col-sm-12">
                                <input id="weight" type="text" class="form-control" name="weight" value="{{ old('weight') }}" required autofocus>

                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
                            <label for="height" class="col-sm-12">height</label>

                            <div class="col-sm-12">
                                <input id="height" type="text" class="form-control" name="height" value="{{ old('height') }}" required>

                                @if ($errors->has('height'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                            <label for="blood_group" class="col-sm-12">Blood Group</label>

                            <div class="col-sm-12">
                                <input id="blood_group" type="text" class="form-control" name="blood_group" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blood_group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary register-btn">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
