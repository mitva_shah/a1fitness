@extends('layouts.app')

@section('bodyclass', 'auth-page register-page')

@section('content')
<div class="container">
    <div class="row register-form-wrap">
        <div class="col-md-6 col-md-offset-3 register-form">
            <div class="site-logo">
                <img src="{{ url('/images/light-logo.png') }}" />
            </div>
            <div class="panel panel-trasparent">
                <div class="panel-heading transparent-panel-heading">
                    <a class="login-icon"><i class="fa fa-user-plus"></i></a>
                    <h1>User Registration</h1>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-12 " style="font-size: 16px; color: #b6b8b5;  display: block;  font-weight: 500; text-align: left;">Name</label>

                            <div class="col-sm-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-12">E-Mail Address</label>

                            <div class="col-sm-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-12">Password</label>

                            <div class="col-sm-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-12">Confirm Password</label>

                            <div class="col-sm-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                          <div class = "form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                            <label for="avatar" class="col-sm-12">Avatar</label>

                            <div class = "col-sm-12">
                              <input type="file" name="avatar" class = "form-label">
                              @if ($errors->has('avatar'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('avatar') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </div>

                          <div class = "form-group{{ $errors->has('register_as') ? ' has-error' : '' }}">
                            <label for="register_as" class="col-sm-12">Register as:</label>
                            <div class="col-sm-12">
                                  @foreach($roles as $role)
                                      @if($role->name != "admin")
                                      <label class = "radio-inline" style="   font-size: 16px; color: #b6b8b5; font-weight: 500;">
                                        <input id="register_as" type="radio" name="register_as" value = "{{ $role->id }}"  >
                                          {{ $role->display_name }}
                                        </label>
                                      @endif
                                  @endforeach

                                @if ($errors->has('register_as'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('register_as') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary register-btn">
                                    Next
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

             <!-- Already Login -->
            <div class="already-user">
                <span>Don't have an account?</span>
                <a href="{{ url('/login') }}" class="login-here">Login</a>
            </div>
        </div>
    </div>
</div>
@endsection
