@extends('layouts.app')

@section('bodyclass', 'auth-page reset-pass-page')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row forgot-pass">
        <div class="col-md-6 col-md-offset-3">
            <div class="site-logo">
                <img src="{{ url('/images/light-logo.png') }}" />
            </div>
            <div class="panel panel-trasparent">
                <div class="panel-heading transparent-panel-heading">
                     <a class="fp-icon"><i class="fa fa-key"></i></a>
                     <h1>Reset Password</h1>
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label for="email" class="col-sm-12">E-Mail Address</label>
                          <div class="col-sm-12">
                              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type="submit" class="btn btn-primary fp-btn">
                                  Send me Instructions
                              </button>
                          </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
