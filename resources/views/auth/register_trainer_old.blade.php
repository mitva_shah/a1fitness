@extends('layouts.app')

@section('content')
<div class="container">
    <form class = "form-horizontal" role = "form" method = "POST" action = "{{ url('trainer/recurring/payment') }}">
    {{csrf_field()}}

    <input id="name" type="hidden" class="form-control" name="name" value="{{ $user_details['name'] }}">
    <input id="email" type="hidden" class="form-control" name="email" value="{{ $user_details['email'] }}">
    <input id="password" type="hidden" class="form-control" name="password" value="{{ $user_details['password'] }}">
    <input id="role_id" type="hidden" class="form-control" name ="role_id" value = "{{ $user_details['role_id'] }}">
    <input id="avatar" type = "hidden" class = "form-control" name = "avatar" value = "{{ $user_details['avatar'] }}">
    <input type = "hidden" name = "price" id = "price" value = "100"/>
    <input type = "hidden" name = "package_selected" id = "package_selected" />

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                          Choose Package
                            <div style=" float:right; margin-right : 22px;">
                              $&nbsp;&nbsp;<input type = "text" disabled = "disabled" name = "total_price" id = "total_price" placeholder = "Total Price" />
                            </div>
                </div>
                <div class="panel-body">
                  <div id = "default_package" data-price = "{{ $default_package['default_package_price'] }}" >
                      <div class = "panel panel-primary">
                      <div class = "panel-heading">
                        <b>{{ $default_package['default_package_title'] }}</b>
                      </div>
                        <div class = "panel-body">
                            <div class = "row">
                          <div class = "col-md-9">
                              {{ $default_package['default_package_description'] }}
                        </div>
                          <div class = "col-md-3" style = "font-size:70px;">${{ $default_package['default_package_price'] }}</div>
                        </div>
                      </div>
                  </div>
                            @foreach($packages as $id=>$package)
                              <div class = "col-md-6 package" data-id = "{{ $id }}" data-price = "{{ $package['price'] }}" data-name = "{{ $package['title']}}" >
                                  <div class = "panel panel-default">
                                  <div class = "panel-heading">
                                    ${{ $package['price'] }} :
                                    <b>{{ $package['title'] }}</b>
                                  </div>
                                    <div class = "panel-body">
                                      <img src = "{{ $package['image'] }}" style=" height : 100px; width : 100px; margin-right : 5px; float: left; display :box; padding :1px; border : 1px solid gray;"/>
                                      {{ $package['description'] }}
                                    </div>
                                  </div>
                              </div>
                            @endforeach
                                  <div class="form-group">
                                      <div class="col-md-6 col-md-offset-9">
                                          <button type="submit" class="btn btn-primary">
                                              Proceed to pay
                                          </button>
                                      </div>
                                  </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection
@section('javascript')
            <script>
            $(document).ready(function()  {
                    jQuery.fn.extend({
                    addToArray: function(value) {
                        return this.filter(":input").val(function(i, v) {
                            if(v != "")
                            {
                              var arr = v.split(',');
                            } else {
                              var arr = new Array();
                            }

                           arr.push(value);
                           return arr.join(',');
                        }).end();
                    },
                    removeFromArray: function(value) {
                        return this.filter(":input").val(function(i, v) {
                           return $.grep(v.split(','), function(val) {
                                    return val != value;
                                  }).join(',');
                        }).end();
                    }
                });


            var total_price = $('#default_package').data('price');
            $('#total_price').attr('value',total_price);
            $('#price').attr('value',total_price);

          $('.package').click(function(e) {
                        //alert('add on package clicked!');
                        //alert($(this).data('selected'));
                        if($(this).data('selected') == 'selected') {
                          var package_id = $(this).data('id');
                          var package_price = $(this).data('price');
                          var package_name = $(this).data('name');
                          total_price = total_price - package_price;

                          $(this).find('.panel').css('border',"");
                          $(this).removeData('selected','selected');
                          $('#total_price').attr('value',total_price);
                          $('#price').attr('value',total_price);

                          //selected packages input field
                          $('#package_selected').removeFromArray(package_id);
                        } else {
                          var package_id = $(this).data('id');
                          var package_price = $(this).data('price');
                          var package_name = $(this).data('name');

                          total_price = total_price + package_price;

                          $(this).find('.panel').css('border','2px solid red');
                          $(this).data('selected','selected');
                          $('#total_price').attr('value',total_price);
                          $('#price').attr('value',total_price);

                          //selected packages input field
                          $("#package_selected").addToArray(package_id);
                        }
          });

          /*
        $('[class="package"]').click(function(e) {
          //e.stopPropagation(); // added to prevent a new alert every
                               // time the click bubbles to a new parent
          alert('add on package clicked!');
          alert($(this).closest('div[id]').attr('id'));
      });
      */
            });
        </script>
@endsection
