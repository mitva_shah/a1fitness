@extends('layouts.app')

@section('bodyclass', 'auth-page login-page')

@section('content')
<div class="container">
    <div class="row login-form-wrap">
        <div class="col-md-6 col-md-offset-3 login-form">
            <div class="site-logo">
                <img src="{{ url('/images/light-logo.png') }}"/>
            </div>
            <div class="panel panel-trasparent">
                <div class="panel-heading transparent-panel-heading">
                    <a class="login-icon"><i class="fa fa-unlock-alt"></i></a>
                    <h1>Login</h1>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-12">E-Mail Address</label>

                            <div class="col-sm-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-12">Password</label>

                            <div class="col-sm-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" >
                                          <span class = "form-label">Remember Me</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <a class="forgot-pass" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                                <button type="submit" class="btn btn-primary login-btn">
                                    Login
                                </button>

                            </div>
                        </div>
                    </form>

                </div>

                 <!-- Social Login -->
                    <div class="social-login">
                        <p>Or you can Login with one of the following</p>
                        <div class="button-group">
                            <a href="{{ url('/social/login/facebook') }}" class="social-login-btn fb">Log in with <i class="fa fa-facebook-square"></i></a>
                            <a href="{{ url('/social/login/google') }}" class="social-login-btn google">Log in with <i class="fa fa-google-plus-square"></i></a>
                        </div>
                    </div>
            </div>

            <!-- Register -->
            <div class="new-user">
                <span>Don't have an account?</span>
                <a href="{{ url('/register') }}" class="sign-up">Sign Up</a>
            </div>
        </div>
    </div>
</div>
@endsection
