@extends('layouts.app')

@section('bodyclass', 'auth-page register-page')

@section('content')
<div class="container">
    <div class="row register-form-wrap">
        <div class="col-md-6 col-md-offset-3 register-form">
            <div class="site-logo">
                <img src="{{ url('/images/light-logo.png') }}" />
            </div>
            <div class="panel panel-trasparent">
                <div class="panel-heading">
                    <a class="login-icon"><i class="fa fa-user-plus"></i></a>
                    <h1>User Registration</h1>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-12">Name</label>

                            <div class="col-sm-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-12">E-Mail Address</label>

                            <div class="col-sm-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-12">Password</label>

                            <div class="col-sm-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-12">Confirm Password</label>

                            <div class="col-sm-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary register-btn">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

             <!-- Already Login -->
            <div class="already-user">
                <span>Don't have an account?</span>
                <a href="{{ url('/login') }}" class="login-here">Login</a>
            </div>
        </div>
    </div>
</div>
@endsection
