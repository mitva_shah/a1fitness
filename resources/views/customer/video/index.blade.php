@extends('layouts.main')
@section('content')
  <div class = "container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Videos</div>
                <div class="panel-body">
                    <table class = "table">
                      <tr>
                        <th>#no</th>
                        <th>Description</th>
                        <th>Uploaded by trainer</th>
                      </tr>
                    @foreach($videos as $id=>$video)
                    <tr>
                        <td>
                          <a href = "{{ url('customer/play/video/'.$id) }}">{{ $id }}</a>
                        </td>
                        <td>
                           {{ $video['description'] }}
                        </td>
                        <td>
                            {{ $video['trainer'] }}
                        </td>

                      </tr>
                    @endforeach
                    </table>

                </div>
                <?php echo $videos->setPath(url('videos'))->render() ?>
              </div>
            </div>
          </div>
  </div>

@endsection
