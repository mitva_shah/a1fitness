@extends('layouts.main')
  @section('content')
<div class = "container">
      <div class = "row">
          <div class = "col-md-6 col-md-offset-3">
              <div class = "panel panel-default">
                  <div class = "panel-heading">
                      <h1>Upload Video</h1>
                  </div>
    <div class = "panel-body">
        <form class = "form-horizontal" action="{{ url('store/video/'.$user->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

      <div class = "form-group{{ $errors->has('file') ? 'has-error' : '' }}">
          <label for="file" class = "col-sm-12">Upload Video:</label>
        <div class = "col-sm-12">
          <input type="file" name="file" id="file" class="form-control" />
          <br/>

            @if($errors->has('file'))
              <span class = "help-block">
                  <strong>{{ $errors->first('file') }}</strong>
              </span>
            @endif
        </div>
      </div>

        <div class = "form-group">
          <label for = "file" class = "col-sm-12">Description</label>

          <div class = "col-sm-12">
              <textarea class = "form-control" name = "description" rows = "5"></textarea>
          </div>
      </div>


      <div class = "form-group">
          <div class = "col-sm-12">
        <input type="submit" class="form-control button button-primary"  name="submit" value="Submit"  />
      </div>
      </div>

      </form>

  </div>
    </div>
      </div>
        </div>
      </div>
@endsection
