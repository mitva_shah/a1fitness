@extends('layouts.main')
@section('content')
  <div class = "container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Videos</div>
                <div class="panel-body">
                    <table class = "table">
                      <tr>
                        <th>#no</th>
                        <th>Description</th>
                      </tr>
                    @foreach($videos as $video)
                    <tr>
                        <td>
                          <a href = "{{ url('play/video/'.$video->id) }}">{{ $video->id }}</a>
                        </td>
                        <td>
                           {{ $video->description }}
                        </td>

                      </tr>
                    @endforeach
                    </table>
                        <a type = "button" class = "btn btn-primary" href = "{{ url('upload/video') }}">+Add New Video</a>
                </div>
                <?php echo $videos->setPath(url('videos'))->render() ?>
              </div>
            </div>
          </div>
  </div>

@endsection
