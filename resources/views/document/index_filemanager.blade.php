<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/css/style.css') }}" >
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script type="text/javascript">
    // File Picker modification for FCK Editor v2.0 - www.fckeditor.net
   // by: Pete Forde <pete@unspace.ca> @ Unspace Interactive
   var urlobj;

   function BrowseServer(obj)
   {
        urlobj = obj;
        OpenServerBrowser(
        '/filemanager/show',
        screen.width * 0.7,
        screen.height * 0.7 ) ;
   }

   function OpenServerBrowser( url, width, height )
   {
        var iLeft = (screen.width - width) / 2 ;
        var iTop = (screen.height - height) / 2 ;
        var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes" ;
        sOptions += ",width=" + width ;
        sOptions += ",height=" + height ;
        sOptions += ",left=" + iLeft ;
        sOptions += ",top=" + iTop ;
        var oWindow = window.open( url, "BrowseWindow", sOptions ) ;
   }

   function SetUrl( url, width, height, alt )
   {
        document.getElementById(urlobj).value = url ;
        oWindow = null;
   }
   </script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #A9A9A9">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      <div class="main-logo">
                          <img src="{{ url('/images/light-logo.png') }}"/>
                      </div>

                        <!--
                        {{ config('app.name', 'Laravel') }}
                      -->
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())

                            <li><a href="{{ url('/login') }}">Login</a></li>

                            <li><a href="{{ url('/register') }}">Register</a></li>

                        @else
                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" >

                                    {{ Auth::user()->name }} <span class="caret"></span>

                                </a>

                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                    <a href = "{{ url('user/profile/'.Auth::user()->id) }}" >View Profile</a>
                                  </li>
                                  <li>
                                    <a href = "{{ url('message') }}">Send Messages</a>
                                  </li>
                                    <!-- Show option to upload videos only if user role is trainer -->

                                      @if(Auth::user()->role->name == Auth::user()::ROLE_TRAINER)
                                        <li>
                                        <a href = "{{ url('videos') }}">Upload Videos</a>
                                      </li>
                                      @endif

                                      @if(Auth::user()->role->name == Auth::user()::ROLE_CUSTOMER)
                                        <li>
                                        <a href = "{{ url('customer/videos') }}">Show Videos</a>
                                      </li>
                                      @endif


                                      @if(Auth::user()->role->name = Auth::user()::ROLE_TRAINER)
                                      <li>
                                        <a href = "">Upload documents</a>
                                      </li>
                                      @endif

                                      @if(Auth::user()->role->name == Auth::user()::ROLE_CUSTOMER)
                                      <li>
                                        <a href= "">Show Documents</a>
                                      </li>
                                      @endif

                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>

                            </li>

                        @endif
                    </ul>

                </div>
            </div>
        </nav>

        @if(session('msg'))
        <div class = "container">
            <div class = "alert alert-success alert-dismissable">
              <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
                 &times;
              </button>
              {{ session('msg') }}
            </div>
          </div>
        @endif

        @if(session('error'))
        <div class = "container alert alert-danger alert-dismissable">
          <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
             &times;
          </button>
          {{ session('error') }}
        </div>
        @endif

        <button type="button" onclick="BrowseServer('id_of_the_target_input');">Pick Image</button>
        <input type="text" id="id_of_the_target_input"/>


        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>

      @yield('javascript')
      <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      @if(Session::has('message'))
    <script>

      var type = "{{ Session::get('alert-type', 'info') }}";
      switch(type){
          case 'info':
              toastr.info("{{ Session::get('message') }}");
              break;

          case 'warning':
              toastr.warning("{{ Session::get('message') }}");
              break;

          case 'success':
              toastr.success("{{ Session::get('message') }}");
              break;

          case 'error':
              toastr.error("{{ Session::get('message') }}");
              break;
      }
    </script>
    @endif

</body>
</html>
