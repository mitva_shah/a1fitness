<div class="people-list" id="people-list">
    <div class="search" style="text-align: center">
        <a href="{{url('/home')}}" style="font-size:16px; text-decoration:none; color: white;"><i class="fa fa-user"></i> {{auth()->user()->name}}</a>
    </div>
    <ul class="list">
        @foreach($threads as $inbox)
            @if(!is_null($inbox->thread))
              <li class="clearfix">
                  <a href="{{route('message.read', ['id'=>$inbox->withUser->id])}}">
                      <img src = "{{ url('/').'/storage/'.$inbox->withUser->avatar}}" alt = "avatar" class = "avatar" />
                  <div class="about">
                      <div class="name">{{$inbox->withUser->name}}
                              @if($inbox->withUser->notification)
                                <span class = "new_messages">{{ "("}}{{ $thread['unread_messages'] }}{{")" }}</span>
                                @endif
                      </div>
                      <div class="status">
                          @if(auth()->user()->id == $inbox->thread->sender->id)
                              <span class="fa fa-reply"></span>
                          @endif
                          <span>{{substr($inbox->thread->message, 0, 20)}}</span>
                      </div>
                  </div>
                  </a>
              </li>
            @endif
        @endforeach
    </ul>
</div>
