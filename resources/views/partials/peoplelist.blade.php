<div class="people-list" id="people-list">
    <div class="search" style="text-align: center">
        <a href="{{url('/home')}}" style="font-size:16px; text-decoration:none; color: white;"><i class="fa fa-user"></i> {{auth()->user()->name}}</a>
    </div>
    <ul class="list">
      @foreach($thread_list as $id=>$thread)
            <li class="clearfix" class = "chat-selected-list">
                <a href="{{route('message.read', ['id'=>$id ])}}" class = "chat-selected">
                    <img src = "{{ url('/').'/storage/'.$thread['avatar']}}" alt = "avatar" class = "avatar-peoplelist" />
                <div class="about">
                    <div class="name">{{$thread['name']}}
                              <span class = "new_messages">{{ "(" }}{{ $thread['unread_messages'] }}{{ ")" }}</span>
                    </div>
                    <div class="status">
                            <span class="fa fa-reply"></span>
                        <span>{{$thread['last_message']}}</span>
                    </div>
                </div>
                </a>
            </li>
      @endforeach
    </ul>
</div>
